/*
 *    Copyright 2009-2012 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.scripting.xmltags;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ognl.OgnlException;
import ognl.OgnlRuntime;
import ognl.PropertyAccessor;

import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.ce.ebiz.framework.base.BaseEntity;
import cn.ce.ebiz.framework.base.Pagination;
import cn.ce.ebiz.framework.context.ContextHolder;
import cn.ce.ebiz.framework.context.ContextParameter;

/**
 * @author Clinton Begin
 */
public class DynamicContext {

  public static final String PARAMETER_OBJECT_KEY = "_parameter";
  public static final String DATABASE_ID_KEY = "_databaseId";

  static {
    OgnlRuntime.setPropertyAccessor(ContextMap.class, new ContextAccessor());
  }

  private final ContextMap bindings;
  private final StringBuilder sqlBuilder = new StringBuilder();
  private int uniqueNumber = 0;
  
  public static Logger logger = LoggerFactory.getLogger(DynamicContext.class);

  public DynamicContext(Configuration configuration, Object parameterObject) {
    if (parameterObject != null && !(parameterObject instanceof Map)) {
      MetaObject metaObject = configuration.newMetaObject(parameterObject);
      bindings = new ContextMap(metaObject);
      
    } else {
      bindings = new ContextMap(null);
    }
    /*if(!bindings.containsKey(ContextParameter.VIEW_TYPE_PROPERTY)){
    	 Integer viewType = ContextHolder.getContext().getViewType();
    	 logger.debug("bindings did not include viewType, viewType from context is "+viewType);
    	 if(null != viewType){
    		 bindings.put(ContextParameter.VIEW_TYPE_PROPERTY, viewType);
    	 }
    }else{
    	logger.debug("bindings include viewType , viewType="+bindings.get(ContextParameter.VIEW_TYPE_PROPERTY));
    }*/
    if(!bindings.containsKey(ContextParameter.SOURCE_APP_PROPERTY)){
   	 Integer sourceApp = ContextHolder.getContext().getSourceApp();
   	 if(null != sourceApp){
   		 bindings.put(ContextParameter.SOURCE_APP_PROPERTY, sourceApp);
   	 }
   }
    //add useViewType property
    if(!bindings.containsKey(ContextParameter.USE_VIEW_TYPE)){
      	 boolean useViewType = ContextHolder.getContext().isUseViewType();
      	 logger.debug("bindings did not include useViewType, useViewType from context is "+useViewType);
      	 bindings.put(ContextParameter.USE_VIEW_TYPE, useViewType);
      	 
    }else{
    	logger.debug("bindings include useViewType , useViewType is "+bindings.get(ContextParameter.USE_VIEW_TYPE)+", value from context is "+ContextHolder.getContext().isUseViewType());
    	
    }
    
    /*//add appId
    if(!bindings.containsKey(ContextParameter.APP_ID_PROPERTY)){
      	 Long appId = ContextHolder.getContext().getAppId();
      	 if(null != appId){
      		 bindings.put(ContextParameter.APP_ID_PROPERTY, appId);
      	 }
      }*/
    
    
    bindings.put(PARAMETER_OBJECT_KEY, parameterObject);
    bindings.put(DATABASE_ID_KEY, configuration.getDatabaseId());
    
    if(bindings.containsKey("appId")){
    	logger.debug("bindings include appId .");
    	Long contextAppId = ContextHolder.getContext().getAppId();
    	logger.debug("contextAppId = "+contextAppId);
    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
    	if(obj == null){
    		logger.debug(PARAMETER_OBJECT_KEY +"==> value is null.");
    		if(contextAppId == null){
    			bindings.remove("appId");
    		}else{
    			bindings.put("appId", contextAppId);
    		}
    	}else{
    		if (obj instanceof Map) {
    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
    	    	  if(maps.containsKey("appId")){
    	    		  bindings.put("appId", maps.get("appId"));
    	    	  }else{
    	    		  bindings.remove("appId");
    	    	  }
    	    }else if(obj instanceof BaseEntity){
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is BaseEntiry type.");
    	    	BaseEntity entity = (BaseEntity) obj;
    	    	if(entity.getAppId() != null){
    	    		bindings.put("appId", entity.getAppId());
    	    	}else{
    	    		 bindings.remove("appId");
    	    	}
    	    }else if(obj instanceof Pagination){
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
    	    	Pagination p = (Pagination) obj;
    	    	Long appId = (Long) p.getParamValue("appId");
    	    	if(appId != null){
    	    		bindings.put("appId", appId);
    	    	}else{
    	    		 bindings.remove("appId");
    	    	}
    	    }else{
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type.");
    	    	if(contextAppId == null){
        			bindings.remove("appId");
        		}else{
        			bindings.put("appId", contextAppId);
        		}
    	    }
    	}
    }else{
    	logger.debug("bindings did not include appId .");
    	Long contextAppId = ContextHolder.getContext().getAppId();
    	logger.debug("contextAppId = "+contextAppId);
    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
    	if(obj == null){
    		logger.debug(PARAMETER_OBJECT_KEY +"==> value is null.");
    		if(contextAppId != null){
    			bindings.put("appId", contextAppId);
    		}
    	}else{
    		if (obj instanceof Map) {
    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
    	    	  if(!maps.containsKey("appId")){
    	    		  if(contextAppId != null){
    	      			bindings.put("appId", contextAppId);
    	      		  }
    	    	  }
    	    }else if(obj instanceof BaseEntity){
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is BaseEntiry type.");
    	    	BaseEntity entity = (BaseEntity) obj;
    	    	if(entity.getAppId() == null){
    	    		if(contextAppId != null){
    	      			bindings.put("appId", contextAppId);
    	      		}
    	    	}
    	    	
    	    }else if(obj instanceof Pagination){
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
    	    	Pagination p = (Pagination) obj;
    	    	Long appId = (Long) p.getParamValue("appId");
    	    	if(appId == null){
    	    		if(contextAppId != null){
    	      			bindings.put("appId", contextAppId);
    	      			p.putParamValue("appId", contextAppId);
    	      		}
    	    	}
    	    }else{
    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type .");
    	    	if(contextAppId != null){
	      			bindings.put("appId", contextAppId);
	      		}
    	    }
    	}
    	
    }
  //增加绑定viewType的方法
    bindViewType();
    //绑定pcstatus
    bindSiteStatus(ContextParameter.PC_RUNNING_PROPERTY);
    bindSiteStatus(ContextParameter.MOBILE_RUNNING_PROPERTY);
    
    if(bindings != null && !bindings.isEmpty()){
    	Set<String> keys = bindings.keySet();
    	logger.debug("==bindings content start==");
    	for(String key : keys){
    		logger.debug("key="+key+", value="+bindings.get(key));
    	}
    	logger.debug("==bindings content end==");
    }
  }
  
  private void bindViewType(){
	  if(bindings.containsKey("viewType")){
	    	logger.debug("bindings include viewType .");
	    	Integer viewType = ContextHolder.getContext().getViewType();
	    	logger.debug("viewTyp from ContextHolder = "+viewType);
	    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
	    	if(obj == null){
	    		logger.debug(PARAMETER_OBJECT_KEY +"==> value is null.");
	    		if(viewType == null){
	    			bindings.remove("viewType");
	    		}else{
	    			bindings.put("viewType", viewType);
	    		}
	    	}else{
	    		if (obj instanceof Map) {
	    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
	    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
	    	    	  if(maps.containsKey("viewType")){
	    	    		  bindings.put("viewType", maps.get("viewType"));
	    	    		  logger.debug("[viewType] of map is "+maps.get("viewType")+", the value is binded to bindings.");
	    	    	  }else{
	    	    		  bindings.remove("viewType");
	    	    		  logger.debug("remove viewType of bindings.");
	    	    	  }
	    	    }else if(obj instanceof BaseEntity){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is BaseEntiry type.");
	    	    	BaseEntity entity = (BaseEntity) obj;
	    	    	if(entity.getViewType() != null){
	    	    		bindings.put("viewType", entity.getViewType());
	    	    		logger.debug("[viewType] of entity is "+entity.getViewType()+", the value is binded to bindings.");
	    	    	}else{
	    	    		 bindings.remove("viewType");
	    	    		 logger.debug("remove viewType of bindings.");
	    	    	}
	    	    }else if(obj instanceof Pagination){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
	    	    	Pagination p = (Pagination) obj;
	    	    	viewType = (Integer) p.getParamValue("viewType");
	    	    	if(viewType != null){
	    	    		bindings.put("viewType", viewType);
	    	    		logger.debug("[viewType] of pagination is "+viewType+", the value is binded to bindings.");
	    	    	}else{
	    	    		 bindings.remove("viewType");
	    	    		 logger.debug("remove viewType of bindings.");
	    	    	}
	    	    }else{
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type.");
	    	    	if(viewType == null){
	        			bindings.remove("viewType");
	        		}else{
	        			bindings.put("viewType", viewType);
	        			logger.debug("[viewType] of cotextHolder is binded to bindings ,the value is " + viewType);
	        		}
	    	    }
	    	}
	    }else{
	    	logger.debug("bindings did not include viewType .");
	    	Integer viewType = ContextHolder.getContext().getViewType();
	    	logger.debug("[viewType] from ContextHolder = "+viewType);
	    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
	    	if(obj == null){
	    		logger.debug(PARAMETER_OBJECT_KEY +"==> value is null.");
	    		if(viewType != null){
	    			bindings.put("viewType", viewType);
	    		}
	    	}else{
	    		if (obj instanceof Map) {
	    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
	    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
	    	    	  if(!maps.containsKey("viewType")){
	    	    		  if(viewType != null){
	    	      			bindings.put("viewType", viewType);
	    	      			logger.debug("[viewType] of contextHolder is binded to bindings, viewType = "+viewType);
	    	      		  }
	    	    	  }else{
	    	    		  logger.debug("Parameter object map include viewType, the value is "+maps.get("viewType"));
	    	    	  }
	    	    }else if(obj instanceof BaseEntity){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is BaseEntiry type.");
	    	    	BaseEntity entity = (BaseEntity) obj;
	    	    	if(entity.getViewType() == null){
	    	    		if(viewType != null){
	    	      			bindings.put("viewType", viewType);
	    	      			logger.debug("[viewType] of contextHolder is binded to bindings, viewType = "+viewType);
	    	      		}
	    	    	}else{
	    	    		logger.debug("Entity object entity include viewType, the value is "+entity.getViewType());
	    	    	}
	    	    	
	    	    }else if(obj instanceof Pagination){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
	    	    	Pagination p = (Pagination) obj;
	    	    	Integer curViewType = (Integer) p.getParamValue("viewType");
	    	    	if(curViewType == null){
	    	    		if(viewType != null){
	    	      			bindings.put("viewType", viewType);
	    	      			p.putParamValue("viewType", viewType);
	    	      			logger.debug("[viewType] of contextHolder is binded to bindings, viewType = "+viewType);
	    	      		}
	    	    	}else{
	    	    		bindings.put("viewType", curViewType);
	    	    		logger.debug("Pagination object include viewType, bind viewType to bindings, the value is "+curViewType);
	    	    	}
	    	    }else{
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type .");
	    	    	if(viewType != null){
		      			bindings.put("viewType", viewType);
		      			logger.debug("[viewType] of contextHolder is binded to bindings , the value is "+viewType);
		      		}
	    	    }
	    	}
	    	
	    }
  }
  private void setValue(Map bindings,Object status,String pcOrmobileRuningProperty){
	  if(status == null){
		  return;
	  }
	  logger.debug("### status is {} !",status);
	  if(status instanceof Boolean){
		  logger.debug("## status is boolean!");
			bindings.put(pcOrmobileRuningProperty, (Boolean)status);
		}else if(status instanceof String && (((String)status).equals("true") || ((String)status).equals("false"))){
			logger.debug("### status is String!");
			bindings.put(pcOrmobileRuningProperty, Boolean.parseBoolean((String)status));
		}else{
			logger.debug("### status is other !"+status);
			bindings.put(pcOrmobileRuningProperty, Boolean.FALSE);
		}
  }
  private void bindSiteStatus(String pcOrmobileRuningProperty){
	  if(pcOrmobileRuningProperty == null || (!pcOrmobileRuningProperty.equals(ContextParameter.PC_RUNNING_PROPERTY) && !pcOrmobileRuningProperty.equals(ContextParameter.MOBILE_RUNNING_PROPERTY))){
		  return ;
	  }
	  if(bindings.containsKey(pcOrmobileRuningProperty)){
	    	logger.debug("### bindings include [{}] !",pcOrmobileRuningProperty);
	    	Object status = ContextHolder.getContext().get(pcOrmobileRuningProperty);
	    	logger.debug("## {} from ContextHolder = {} !",pcOrmobileRuningProperty,status);
	    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
	    	if(obj == null){
	    		logger.debug("### " +PARAMETER_OBJECT_KEY +"==> value is null.");
	    		if(status == null){
	    			bindings.remove(pcOrmobileRuningProperty);
	    		}else{
	    			if(status instanceof Boolean){
	    				bindings.put(pcOrmobileRuningProperty, (Boolean)status);
	    			}else if(status instanceof String && (((String)status).equals("true") || ((String)status).equals("false"))){
	    				bindings.put(pcOrmobileRuningProperty, Boolean.parseBoolean((String)status));
	    			}else{
	    				bindings.put(pcOrmobileRuningProperty, Boolean.FALSE);
	    			}
	    			
	    		}
	    	}else{
	    		if (obj instanceof Map) {
	    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
	    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
	    	    	  if(maps.containsKey(pcOrmobileRuningProperty)){
	    	    		  setValue(bindings,maps.get(pcOrmobileRuningProperty),pcOrmobileRuningProperty);
	    	    		  logger.debug("["+pcOrmobileRuningProperty+"] of map is "+maps.get(pcOrmobileRuningProperty)+", the value is binded to bindings.");
	    	    	  }else{
	    	    		  bindings.remove(pcOrmobileRuningProperty);
	    	    		  logger.debug("remove {} of bindings.",pcOrmobileRuningProperty);
	    	    	  }
	    	    }else if(obj instanceof Pagination){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
	    	    	Pagination p = (Pagination) obj;
	    	    	status =  p.getParamValue(pcOrmobileRuningProperty);
	    	    	if(status != null){
	    	    		setValue(bindings,status,pcOrmobileRuningProperty);
	    	    		logger.debug("["+pcOrmobileRuningProperty+"] of pagination is "+status+", the value is binded to bindings.");
	    	    	}else{
	    	    		 bindings.remove(pcOrmobileRuningProperty);
	    	    		 logger.debug("### remove {} of bindings.",pcOrmobileRuningProperty);
	    	    	}
	    	    }else{
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type.");
	    	    	if(status == null){
	        			bindings.remove(pcOrmobileRuningProperty);
	        		}else{
	        			setValue(bindings,status,pcOrmobileRuningProperty);
	        			//bindings.put(pcOrmobileRuningProperty, viewType);
	        			logger.debug("### ["+pcOrmobileRuningProperty+"] of cotextHolder is binded to bindings ,the value is " + status);
	        		}
	    	    }
	    	}
	    }else{
	    	logger.debug("### bindings did not include {} !",pcOrmobileRuningProperty);
	    	Object status = ContextHolder.getContext().get(pcOrmobileRuningProperty);
	    	logger.debug("## {} from ContextHolder = {} !",pcOrmobileRuningProperty,status);
	    	Object obj = bindings.get(PARAMETER_OBJECT_KEY);
	    	if(obj == null){
	    		logger.debug(PARAMETER_OBJECT_KEY +"==> value is null.");
	    		if(status != null){
	    			setValue(bindings,status,pcOrmobileRuningProperty);
	    		}
	    	}else{
	    		if (obj instanceof Map) {
	    			logger.debug(PARAMETER_OBJECT_KEY +"==> value is map");
	    	    	  Map<String,Object> maps =  (Map<String,Object>)obj;
	    	    	  if(!maps.containsKey(pcOrmobileRuningProperty)){
	    	    		  if(status != null){
	    	    			  setValue(bindings,status,pcOrmobileRuningProperty);
	    	      			 logger.debug("["+pcOrmobileRuningProperty+"] of contextHolder is binded to bindings, the value = "+status);
	    	      		  }
	    	    	  }else{
	    	    		  logger.debug("Parameter object map include "+pcOrmobileRuningProperty+", the value is "+maps.get(pcOrmobileRuningProperty));
	    	    	  }
	    	    }else if(obj instanceof Pagination){
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is Pagination type.");
	    	    	Pagination p = (Pagination) obj;
	    	    	Object curViewType =  p.getParamValue(pcOrmobileRuningProperty);
	    	    	if(curViewType == null){
	    	    		if(status != null){
	    	    			setValue(bindings,status,pcOrmobileRuningProperty);
	    	    			setValue(p.getParams(),status,pcOrmobileRuningProperty);
	    	      			//p.putParamValue(pcOrmobileRuningProperty, viewType);
	    	      			logger.debug("["+pcOrmobileRuningProperty+"] of contextHolder is binded to bindings, "+pcOrmobileRuningProperty+" = "+status);
	    	      		}
	    	    	}else{
	    	    		//bindings.put("viewType", curViewType);
	    	    		setValue(bindings,curViewType,pcOrmobileRuningProperty);
	    	    		logger.debug("Pagination object include "+pcOrmobileRuningProperty+",  the value is "+curViewType);
	    	    	}
	    	    }else{
	    	    	logger.debug(PARAMETER_OBJECT_KEY +"==> value is other type .");
	    	    	if(status != null){
	    	    		setValue(bindings,status,pcOrmobileRuningProperty);
		      			logger.debug("["+pcOrmobileRuningProperty+"] of contextHolder is binded to bindings , the value is "+status);
		      		}
	    	    }
	    	}
	    	
	    }
  }

  public Map<String, Object> getBindings() {
    return bindings;
  }

  public void bind(String name, Object value) {
    bindings.put(name, value);
  }

  public void appendSql(String sql) {
    sqlBuilder.append(sql);
    sqlBuilder.append(" ");
  }

  public String getSql() {
    return sqlBuilder.toString().trim();
  }

  public int getUniqueNumber() {
    return uniqueNumber++;
  }

  static class ContextMap extends HashMap<String, Object> {
    private static final long serialVersionUID = 2977601501966151582L;

    private MetaObject parameterMetaObject;
    public ContextMap(MetaObject parameterMetaObject) {
      this.parameterMetaObject = parameterMetaObject;
    }

    @Override
    public Object put(String key, Object value) {
      return super.put(key, value);
    }
    
    @Override
    public Object get(Object key) {
      String strKey = (String) key;
      if (super.containsKey(strKey)) {
        return super.get(strKey);
      }

      if (parameterMetaObject != null) {
        Object object = parameterMetaObject.getValue(strKey);
        // issue #61 do not modify the context when reading
//        if (object != null) { 
//          super.put(strKey, object);
//        }

        return object;
      }

      return null;
    }
  }

  static class ContextAccessor implements PropertyAccessor {

    public Object getProperty(Map context, Object target, Object name)
        throws OgnlException {
      Map map = (Map) target;

      Object result = map.get(name);
      if (result != null) {
        return result;
      }

      Object parameterObject = map.get(PARAMETER_OBJECT_KEY);
      if (parameterObject instanceof Map) {
    	  return ((Map)parameterObject).get(name);
      }

      return null;
    }

    public void setProperty(Map context, Object target, Object name, Object value)
        throws OgnlException {
      Map map = (Map) target;
      map.put(name, value);
    }
  }
}